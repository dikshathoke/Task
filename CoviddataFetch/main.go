package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type City struct {
	Pincode  string `json:"pincode"`
	Wheather string `json:"wheather"`
} 

var cities []City

func getCity(w http.ResponseWriter, r *http.Request) { 
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(cities)
}

func main() {
	r := mux.NewRouter()

	cities = append(cities, City{Pincode: "421501", Wheather: "Raining"})
	cities = append(cities, City{Pincode: "421503", Wheather: "HazeCloud"})
	r.HandleFunc("/cities", getCity).Methods("GET")

	fmt.Printf("Strating server at port 8000\n")
	log.Fatal(http.ListenAndServe(":8000", r))
}
