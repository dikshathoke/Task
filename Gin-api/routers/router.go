package routers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type User struct {
	Name string `json:"name"`
	City string `json:"city"`  
}
type Users []User

func Routes() *gin.Engine {
	users := Users{
		{Name: "Diksha", City: "Nasik"},
		{Name: "Shubham", City: "Pune"},
	}

	r := gin.Default()

	r.GET("users", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, users)
	})
	return r
}
