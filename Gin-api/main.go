package main

import (
	"Gin-api/routers"
)

func main() {
	routes := routers.Routes()

	routes.Run(":8000")
}
